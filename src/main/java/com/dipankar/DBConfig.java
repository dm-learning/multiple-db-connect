package com.dipankar;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class DBConfig {
	
	@Bean(name = "ds1")
	@ConfigurationProperties("testdb.datasource")
	public DataSource datasource1() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "ds2")
	@ConfigurationProperties("hfsdb.datasource")
	public DataSource datasource2() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "jdbcTemplate1") 
    public JdbcTemplate jdbcTemplate1(DataSource ds1) { 
        return new JdbcTemplate(ds1); 
    } 
	
	@Bean(name = "jdbcTemplate2") 
    public JdbcTemplate jdbcTemplate2(DataSource ds2) { 
        return new JdbcTemplate(ds2); 
    }
	
	@Bean ( name = "hfsDbTransactionManager")
    public PlatformTransactionManager hfsDBTransactionManager(DataSource ds2 ) {
    	DataSourceTransactionManager hfsDBTransactionManager = new DataSourceTransactionManager();
    	hfsDBTransactionManager.setDataSource(ds2);
    	return hfsDBTransactionManager;
    }
	
	@Bean(name = "chainedTransactionManager")
    public ChainedTransactionManager transactionManager(@Qualifier("oracleTransactionManager") PlatformTransactionManager ds1,
                                                    @Qualifier("hfsDbTransactionManager") PlatformTransactionManager ds2) {
         return new ChainedTransactionManager(ds1, ds2);
    }
	
}
