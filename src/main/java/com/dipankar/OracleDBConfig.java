package com.dipankar;

import oracle.jdbc.pool.OracleDataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import java.sql.SQLException;

@Configuration
@ConfigurationProperties("oracle")
public class OracleDBConfig {
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String url;
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    @Bean(name= "oracleDataSource")
    DataSource oracleDataSource() throws SQLException {
        OracleDataSource oracleDataSource = new OracleDataSource();
        oracleDataSource.setUser(username);
        oracleDataSource.setPassword(password);
        oracleDataSource.setURL(url);
        oracleDataSource.setImplicitCachingEnabled(true);
        oracleDataSource.setFastConnectionFailoverEnabled(true);
        return oracleDataSource;
    }
    
    @Bean(name = "oracleJdbcTemplate") 
    public JdbcTemplate oracleJdbcTemplate(DataSource oracleDataSource) { 
        return new JdbcTemplate(oracleDataSource); 
    }
    
    @Bean ( name = "oracleTransactionManager")
    public PlatformTransactionManager oracleTransactionManager(DataSource oracleDataSource ) {
    	DataSourceTransactionManager oracleTransactionManager = new DataSourceTransactionManager();
    	oracleTransactionManager.setDataSource(oracleDataSource);
    	return oracleTransactionManager;
    }
   
    
}