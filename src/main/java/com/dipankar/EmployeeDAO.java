package com.dipankar;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDAO {
	
	@Autowired 
    @Qualifier("jdbcTemplate1") 
    private JdbcTemplate jdbcTemplate;
	
	public List<Employee> getEmployeeInfo() {
		
		List<Employee> empList = new ArrayList();
		
		jdbcTemplate.query(
                "select * from employee", 
                (rs, rowNum) -> new Employee(rs.getInt("emp_id"), rs.getString("emp_name"), rs.getString("dept"))
        ).forEach(emp -> empList.add(emp));
		
		return empList;
	}
	

}
