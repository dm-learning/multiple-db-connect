package com.dipankar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private EmployeeDAO empDAO;
	
	@Autowired 
	private OracleUserDAO oracleUserDAO;
	
	@GetMapping(value = "/user")
	public List getUserInfo() {
		return userDAO.getUserInfo();
		
	}
	
	@GetMapping(value = "/employee")
	public List getEmployeeInfo() {
		return empDAO.getEmployeeInfo();
	}
	
	@GetMapping(value = "/oracleUser")
	public List getOracleUserInfo() {
		return oracleUserDAO.getUserInfo();
	}
	
	@GetMapping(value = "/addUser")
	public String addUser() {
		oracleUserDAO.createDBRecords();
		return "Record created";
	}
	

}
