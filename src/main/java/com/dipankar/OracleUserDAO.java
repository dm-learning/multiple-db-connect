package com.dipankar;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class OracleUserDAO {
	
	@Autowired 
    @Qualifier("oracleJdbcTemplate") 
    private JdbcTemplate oracleJdbcTemplate;
	
	@Autowired 
	@Qualifier("jdbcTemplate2") 
	private JdbcTemplate jdbcTemplate2;
	
	public List<User> getUserInfo() {
		
		List<User> users = new ArrayList();
		
		oracleJdbcTemplate.query(
                "select * from new_user", 
                (rs, rowNum) -> new User(rs.getString("user_name"), rs.getString("password"), rs.getString("display_name"))
        ).forEach(user -> users.add(user));
		
		return users;
	}
	
	@Transactional (value = "chainedTransactionManager")
	public void createDBRecords() {
		
		String userInsert = "insert into new_user(user_name,password,display_name) values (?,?,?)";
		String userInsert1 = "insert into user(user_name,password,display_name) values (?,?,?)";
		oracleJdbcTemplate.update(userInsert, new Object[] { "user1","pass1","desc1"});
		jdbcTemplate2.update(userInsert1, new Object[] { "user1","pass1","desc1"});
		
	}

}